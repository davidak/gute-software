# Gute Software

Dieses Projekt hat zum Ziel, eine Informationsseite für Benutzer zur Verfügung zu stellen.

Die Idee kam mir, da ein Freund die Domain gutesoftware.de hat, aber keinen Inhalt dafür. Da ich in der Qualitätssicherung arbeite und mich auch in der Freizeit mit der Verbesserung von Freier Software beschäftige, habe ich eine Meinung was gute Software ist und auch Interesse das Thema umfassend zu erforschen und differenziert darzustellen.

## User Story

Als Software-Benutzer
möchte ich Software-Projekte einfach bewerten können,
um mich für das Beste zu entscheiden.

### Ziele

- Benutzer können lernen, was gute Software ausmacht
- Benutzer können Software durch eine Checkliste prüfen
- Es gibt eine Bewertung von bekannter Software
- Diese Bewertung ist auch als strukturierte Daten verfügbar, so dass sie in anderen Projekten verwendet werden können
- Das Projekt ist in mehreren Sprachen verfügbar

### Zielgruppe

- Jeder Mensch, der Software benutzt

Eventuell lässt sich die Zielgruppe später weiter eingrenzen, um diese Gruppe mit Marketing gezielt anzusprechen.

## Betreiber

Dieses Projekt wird von [davidak](https://davidak.de/) betrieben.

## Beitragen

Damit die Informationen vollständig und korrekt sind ist Feedback sehr hilfreich.

Die Diskussionen finden in den [Issues](https://codeberg.org/davidak/gute-software/issues/) statt. Wenn es dir lieber ist kannst du auch eine E-Mail an info at gutesoftware.de schreiben.

### Entwicklung

Die Webseite wird mit dem Static Site Generator [Hugo](https://gohugo.io/) erstellt.

Eine reproduzierbare Entwicklungsumgebung (gepinnte Version) bekommst du mit dem [Nix package manager](https://nixos.org/nix/) und dem Befehl:

    nix-shell

Um die statische HTML-Seite zu erzeugen wird folgender Befehl verwendet:

    hugo

Mit `hugo serve` wird die Seite im RAM erzeugt und über einen lokalen Webserver aufrufbar gemacht. Bei Änderungen werden die entsprechenden Seiten automatisch neu erzeugt und geladen. Das ist bei der Entwicklung sehr praktisch!

Sie kann mit rsync auf den Server hochgeladen werden.

    rsync -rah --progress --delete public/ gutesoftware@atomic.davidak.de:web/

## Lizenz

Copyright (C) 2019 davidak

Veröffentlicht unter der [CC-BY-SA 4.0 International](LICENSE)-Lizenz.

Das Theme basiert auf [hugo-theme-basic](https://github.com/siegerts/hugo-theme-basic/) und steht unter der [MIT](themes/hugo-theme-basic/LICENSE)-Lizenz.
