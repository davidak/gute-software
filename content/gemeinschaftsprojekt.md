---
title: "Gemeinschaftsprojekt Definition"
date: 2021-01-20T13:30:00+01:00
draft: false
---

Folgende Kriterien müssen erfüllt sein, um als Gemeinschaftsprojekt im Sinne unserer Definition zu gelten.

## Community Project 0.1

Die Schlüsselwörter "MUSS", "DARF NICHT", "ERFORDERLICH", "SOLL",
"VERBOTEN", "NÖTIG", "NICHT NÖTIG", "SOLL NICHT", "EMPFOHLEN", "DARF",
"KANN" und "OPTIONAL" werden nach 2119de interpretiert.
[github.com/adfinis-sygroup/2119](https://github.com/adfinis-sygroup/2119/blob/master/2119de.rst) ([Archiv](https://web.archive.org/web/20191110020457/https://github.com/adfinis-sygroup/2119/blob/master/2119de.rst))

- Ein Gemeinschaftsprojekt MUSS als solches erkennbar sein, z.B. durch einen Hinweis auf der Webseite oder in der README. "This is a community effort" genügt dafür.

- Die Ziele des Projektes MÜSSEN erkennbar sein, z.B. in der README. Entscheidungen MÜSSEN entsprechend den Zielen getroffen werden und nicht etwa nach der persönlichen Meinung eines Maintainers. Hier hilft es, wenn es mehrere Maintainer gibt und jeder ein Veto-Recht hat. Auch MUSS auf Beiträge aus der Gemeinschaft entsprechend den Zielen reagiert werden.

- Die Maintainer MÜSSEN auf Beiträge aus der Gemeinschaft eingehen. Das MUSS für die gesamte Gemeinschaft sichtbar sein und bleiben. Kritik DARF NICHT gelöscht werden.

- Fehlerbehebungen MÜSSEN von den Maintainern akzeptiert werden, wenn die Qualität zufriedenstellend ist. Wenn die Qualität zu schlecht ist MÜSSEN die Maintainer Feedback geben, was verbessert werden muss, damit es akzeptiert wird.

## Erklärung der Begriffe

Ein Gemeinschaftsprojekt grenzt sich durch seine Ziele und Verwaltung von kommerziellen oder Hobby-Projekten ab. Das Ziel eines kommerziellen Projektes ist der Profit. Ein Hobby-Projekt wird meist von einem einzelnen Menschen entwickelt für dessen Bedarf, um zu lernen oder rein aus Spaß an der Software-Entwicklung. Wenn ein Projekt Freie Software ist, von einem kommerziellen- oder Hobby-Projekt ein Fork als Gemeinschaftsprojekt gemacht werden.

Die Gemeinschaft eines Projektes besteht aus den Maintainern, Beitragenden und Benutzern.

Maintainer verwalten das Projekt. Sie haben mehr Rechte und Verantwortung als Beitragende. So können sie Änderungen am Code von Beitragenden akzeptieren und zum Projekt hinzufügen. Auch treffen sie Entscheidungen und haben Kontrolle über die Infrastraktur des Projektes.

Beitragende sind Menschen, die in irgend einer Weise zur Verbesserung des Projektes beitragen. Das können Spenden sein, um die Entwicklung zu finanzieren, Fehlerberichte, Verbesserungsvorschläge, Kritik, Verbesserungen am Code, Übersetzungen, oder Andere.

Benutzer sind Menschen, die die Software verwenden.
