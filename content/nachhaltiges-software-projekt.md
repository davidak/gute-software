---
title: "Nachhaltiges Software-Projekt Definition"
date: 2021-01-20T13:47:00+01:00
draft: false
---

Folgende Kriterien müssen erfüllt sein, um als Nachhaltiges Software-Projekt im Sinne unserer Definition zu gelten.

## Sustainable Software Project 0.1

Die Schlüsselwörter "MUSS", "DARF NICHT", "ERFORDERLICH", "SOLL",
"VERBOTEN", "NÖTIG", "NICHT NÖTIG", "SOLL NICHT", "EMPFOHLEN", "DARF",
"KANN" und "OPTIONAL" werden nach 2119de interpretiert.
[github.com/adfinis-sygroup/2119](https://github.com/adfinis-sygroup/2119/blob/master/2119de.rst) ([Archiv](https://web.archive.org/web/20191110020457/https://github.com/adfinis-sygroup/2119/blob/master/2119de.rst))

- Das Projekt MUSS mindestens 2 Maintainer haben, um den [Bus-Faktor](https://de.wikipedia.org/wiki/Truck_Number) zu verringern. Wenn es nur einen Maintainer hat, kann es negative Folgen für das Projekt haben, wenn dieser weg ist, z.B. werden Merge Requests nicht bearbeitet (kann durch einen Fork gelöst werden) oder die Domain geht verloren, weil sie nicht bezahlt wird.

- Das Projekt MUSS Benutzern die Möglichkeit geben es finanziell zu unterstützen. Das ist in ihrem Interesse, da so die Weiterentwicklung sichergestellt wird.

- Das Projekt MUSS Benutzer dazu aufrufen monatlich einen kleinen Betrag zu spenden. Je mehr unterstützen, desto weniger muss jeder spenden, um die Finanzierung sicher zu stellen.

- Das Projekt MUSS hauptsächlich direkt durch seine Benutzer finanziert werden.

- Das Projekt DARF NICHT zu mehr als 5% durch eine Person oder Organisation finanziert werden. Das stellt sicher, dass die Software im Interesse aller Benutzer entwickelt wird und nicht im Interesse einzelner großer Sponsoren. Die Finanzierung ist nachhaltig, da sie nicht durch Wegfall einzelner Unterstützer gefährdet ist.
