---
title: "Quellen"
date: 2019-10-13T10:38:40+02:00
draft: false
---

## Webseiten

- https://de.wikipedia.org/wiki/Freie_Software
- https://de.wikipedia.org/wiki/Open_Source
- https://de.wikipedia.org/wiki/Copyleft
- https://de.wikipedia.org/wiki/Freiz%C3%BCgige_Open-Source-Lizenz
- https://de.wikipedia.org/wiki/GNU_General_Public_License
- https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses
- http://lucumr.pocoo.org/2009/2/12/are-you-sure-you-want-to-use-gpl/
- http://lucumr.pocoo.org/2013/7/23/licensing/
- https://reproducible-builds.org/
- https://en.wikipedia.org/wiki/Reproducible_builds
- https://prototypefund.de/project/reproducible-builds-in-der-wirklichkeit/
- https://www.golem.de/news/softwaresicherheit-vertrauen-durch-reproduzierbare-build-prozesse-1502-112092.html
- https://opensource.guide/best-practices/
- https://opensource.guide/finding-users/
- https://bestpractices.coreinfrastructure.org/en
- https://de.wikipedia.org/wiki/Readme
- https://www.makeareadme.com/
- https://keepachangelog.com/de/
- https://de.wikipedia.org/wiki/%C3%84nderungsprotokoll
- https://de.wikipedia.org/wiki/Manpage
- https://en.wikipedia.org/wiki/Man_page
- https://www.ifun.de/software-abos-die-vorteile-fuer-nutzer-aus-entwickler-sicht-126787/
- https://www.docma.info/blog/abo-abo-abo-der-neuezukuenftige-software-standard
- https://www.heise.de/mac-and-i/meldung/Software-Verkauf-Fuer-Apple-stehen-alle-Zeichen-auf-Abo-4138265.html
- https://geekyorganizer.com/software-abos/

## Vorträge

- [Jeder sollte für Infos und Software zahlen](https://media.ccc.de/v/bub2018-163-jeder_sollte_fur_infos_und_software_zahlen) von [Bernhard E. Reiter](https://intevation.de/~bernhard/) ([FSFE](https://fsfe.org/index.de.html))
- [Über die Nachhaltigkeit von Software](https://media.ccc.de/v/thms-49-ber-die-nachhaltigkeit-von-software) von [Erik Albers](https://3rik.cc/) ([FSFE](https://fsfe.org/index.de.html))
- [Konviviale Software vor und jenseits des digitalen Kapitalismus](https://media.ccc.de/v/thms-42-konviviale-software-vor-und-jenseits-des-digitalen-kapitalismus) von Nicolas Guenot ([Konzeptwerk Neue Ökonomie e.V.](https://konzeptwerk-neue-oekonomie.org/))
- [Funding für Open Source – Wer, Wie und Warum?](https://media.ccc.de/v/froscon2017-2005-funding_fur_open_source_wer_wie_und_warum) von [Elisa Lindinger](https://elisalindinger.de/) ([Prototype Fund](https://prototypefund.de/))
- [Free Software Needs Free Tools](https://mako.cc/writing/hill-free_tools.html) von [Benjamin Mako Hill](https://mako.cc/) ([Community Data Science Collective](https://communitydata.cc/))
- [Organisational Structures for Sustainable Free Software Development](https://media.ccc.de/v/34c3-9087-organisational_structures_for_sustainable_free_software_development) von [Moritz Bartl](https://www.headstrong.de/) ([Center for the Cultivation of Technology](https://techcultivation.org/))

## Feedback

- [Frage auf Mastodon](https://chaos.social/@davidak/102925600029344473)
