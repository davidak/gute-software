let

  # See https://nixos.wiki/wiki/FAQ/Pinning_Nixpkgs for more information on pinning
  pinnedPkgs = builtins.fetchTarball {
    # Descriptive name to make the store path easier to identify
    name = "nixos-19.09-2019-10-27";
    # Use latest stable release URL from https://howoldis.herokuapp.com/
    url = https://releases.nixos.org/nixos/19.09/nixos-19.09.941.27a5ddcf747/nixexprs.tar.xz;
    # Hash obtained using `nix-prefetch-url --unpack <url>`
    sha256 = "1pfjbdsnf5k8wgfxq7sqcr42kz9rax113g81p4g7ivx5p9nljx23";
  };

in

  pinnedPkgs
