let
  # use pinned nixpkgs
  nixpkgs = import ./nixpkgs.nix;
in

# This allows overriding pkgs by passing `--arg pkgs ...`
{ pkgs ? import nixpkgs {} }:

with pkgs;

mkShell {
  buildInputs = [
    hugo
  ];

#  shellHook = ''
#    mkdir -p themes
#    ln -snf "${hugo-theme-terminal}" themes/hugo-theme-terminal
#  '';
}
